# BUS - find the things you are looking for #

Getting information on XYZ tech module or product can be an absolute pain:

* is it on Confluence?
* is it on Trac?
* is it somewhere as a comment on SVN?
* is it on a random Bitbucket repo?

This is to unify that search to help finding information quickly

### Broadbean Universal Search ###

A Hackaday Project, July 2017 by

* Jules Decol
* Niccolo Menozzi
* Theo van Hoesel
